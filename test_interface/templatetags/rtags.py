from django.urls import reverse
from django import template

register = template.Library()


@register.simple_tag(name='rurl')
def rurl(url, *args, **kwargs):
    return "/diniro/" + reverse(url)


@register.simple_tag(name='rstatic')
def rstatic(file, *args, **kwargs):
    return "/diniro/static/" + file