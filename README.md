1. Installation
1. Documentation
1. System requirements


 Installation


 Here you can download Diniro to your local computer.

 NOTE that this version of Diniro has been generated for local usage.

 The webtool is in a docker image that is generated using docker-compose. Follow
 this link, if you want to install

 docker-compose: https://docs.docker.com/compose/

 Once this is done, all you need to do is:

 
1.  clone the repository

1.  sudo docker-compose build (takes ~1 hour :-O)

1.  sudo docker-compose up
 
1.  Go to: http://0.0.0.0:8000/


You can then use Diniro LOCALY.
 Enjoy.
 
 Documentation

 See https://exbio.wzw.tum.de/diniro// documentation or install the program.
 
 System requirements
 
 Hardware requirements

 The program requires a standard computer with enough RAM to support the in-memory operations.
 
 OS requirements

 The program has only been tested on:
 
 Linux: Ubuntu: 16.04
 
 However, it should work in other platforms as well.









